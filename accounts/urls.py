from django.urls import path
from .views import user_login, user_logout, user_signup


urlpatterns = [
    path("login/", user_login, name="login"),  # login view path
    path("logout/", user_logout, name="logout"),  # logout view path
    path("signup/", user_signup, name="signup"),  # sign up view path
]
